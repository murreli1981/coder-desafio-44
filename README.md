# coder-desafio-44

Api Restful - Graphql

## Estructura del proyecto:

```
.
├── resources
└── src
    ├── config
    ├── controllers
    │   ├── auth
    │   └── modules
    ├── dal
    │   ├── db
    │   ├── models
    │   └── repositories
    │       └── modules
    ├── graphql
    │   ├── resolvers
    │   └── schema
    ├── interfaces
    │   ├── Controllers
    │   ├── Repositories
    │   ├── Routes
    │   └── Services
    ├── routes
    │   ├── auth
    │   └── modules
    ├── server
    └── services
        ├── auth
        └── modules
```

## Inicio del server

el server se ejecuta corriendo `npx start` que se encargará de transpilar ts y correr node

## Paths:

### Graphql

```
 src/graphql
       ├── resolvers
       └── schema
```

toda la implementación de graphql se encuentra en esta parte:

`resolvers`: métodos que realizan las operaciones
`schema`: types, métodos de inputs, queries y mutations para todas las entidades

```
src/server/server.ts
```

el método config agrega graphql como middleware de express

`localhost:3002/graphql` UI de graphql

### User:

`[GET] /user` devuelve todos los usuarios.

`[GET] /user/:id ` devuelve un usuario por id.

`[POST] /user` crea un usuario.

`[PATCH] /user/:id` actualiza uno o varios campos del usuario.

`[DELETE] /user/:id` borra un producto.

### Product:

`[GET] /product` devuelve todos los producto.

`[GET] /product/:id ` devuelve un producto por id.

`[POST] /product` crea un producto.

`[PATCH] /product/:id` actualiza uno o varios campos del producto.

`[DELETE] /product/:id` borra un producto.

### Message:

`[GET] /message` devuelve todos los mensajes.

`[GET] /message/:id ` devuelve un mensaje por id.

`[POST] /message` crea un mensaje.

`[PATCH] /message/:id` actualiza uno o varios campos del mensaje.

`[DELETE] /message/:id` borra un mensaje.

### Auth

`[POST] /login` authenticación.

### Documentación

en la colección de postman: `resources/coder-house-backend.postman_collection.json`, carpeta: `coder-desafio-44` están todas las queries y mutations de graphql para las 3 entidades [`users`, `products`, `messages`] con datos cargados para probar.

🚨 Todos los endpoints están con el path `localhost:3002` 🚨

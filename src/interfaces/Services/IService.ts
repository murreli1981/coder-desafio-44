export default interface IService {
  listAll(): Promise<any>;
  getByFilter(filter: {}): Promise<any>;
  getElementById(id: String): Promise<any>;
  createElement(element: any): Promise<any>;
  updateElement(id: String, payload: any): Promise<any>;
  deleteElement(id: String): Promise<any>;
}

import { Request, Response } from "express";

export default interface IController {
  getAll(req: Request, res: Response, next: Function): Promise<void>;
  create(req: Request, res: Response, next: Function): Promise<void>;
  getOne(req: Request, res: Response, next: Function): Promise<void>;
  update(req: Request, res: Response, next: Function): Promise<void>;
  delete(req: Request, res: Response, next: Function): Promise<void>;
}

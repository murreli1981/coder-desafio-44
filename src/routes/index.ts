import { messageRouter } from "./modules/message.routes";
import { productRouter } from "./modules/product.routes";
import { userRouter } from "./modules/user.routes";
import { authRouter } from "./auth/auth.routes";

import controllers from "../controllers";

const routers = {
  messageRouter: messageRouter(controllers.messageController),
  productRouter: productRouter(controllers.productController),
  userRouter: userRouter(controllers.userController),
  authRouter: authRouter(controllers.authController),
};

export default routers;

import { Router } from "express";
export const authRouter = (controller: any) => {
  const routes = Router();

  routes.post("/login", controller.login);
  return routes;
};

export default authRouter;

import { Router } from "express";
import IController from "../../interfaces/Controllers/IController";
export const productRouter = (controller: IController) => {
  const routes = Router();

  routes
    .get("/:id", controller.getOne)
    .get("/", controller.getAll)
    .post("/", controller.create)
    .patch("/:id", controller.update)
    .delete("/:id", controller.delete);

  return routes;
};

import Server from "./server/server";
import routers from "./routes";

const server = new Server(routers);
server.start();

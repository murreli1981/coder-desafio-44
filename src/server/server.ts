import config from "../config/globals";
import express, { Application, Router } from "express";
import cors from "cors";
import Connection from "../dal/db/connection";
import { graphqlHTTP } from "express-graphql";
import resolvers from "../graphql/resolvers";
import schemas from "../graphql/schema";
export default class Server {
  private app: Application;
  private router = Router();
  private routers: any;

  constructor(routers: any) {
    this.app = express();
    this.routers = routers;
    this.config();
  }

  private config(): void {
    this.router.use(cors());
    this.router.use(express.json());
    this.router.use("/product", this.routers.productRouter);
    this.router.use("/user", this.routers.userRouter);
    this.router.use("/message", this.routers.messageRouter);
    this.router.use("/", this.routers.authRouter);
    this.app.use(this.router);
    this.app.use(
      "/graphql",
      graphqlHTTP({
        rootValue: resolvers,
        schema: schemas,
        graphiql: true,
      })
    );
  }

  public async start(): Promise<void> {
    await Connection.getConnection();
    this.app.listen(config.PORT, () => {
      console.info(">> Server listening on port:", config.PORT);
    });
  }
}

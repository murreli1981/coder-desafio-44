import { buildSchema } from "graphql";

export default buildSchema(`
type User {
    _id: ID!
    username: String!
    hash: String!
}

type Product {
    _id: ID!
    title: String!
    price: String!
    thumbnail: String!
}

type Message {
    _id: ID!
    message: String!
    date: String!
    author: Author
}

type Author {
    id: String
    nombre: String
    apellido: String
    avatar: String
    edad: Int
    alias: String
}

input AuthorInput {
    id: String
    nombre: String
    apellido: String
    avatar: String
    edad: Int
    alias: String
}

input UserInput {
    username: String
    hash: String
}

input ProductInput {
    title: String
    price: String
    thumbnail: String
}

input MessageInput {
    id: String
    date: String
    message: String
    author: AuthorInput
}

type Query {
    users:[User!]
    user(id: ID!): User
    products:[Product!]
    product(id: ID!): Product
    messages: [Message!]
    message(id: ID!): Message
}

type Mutation {
    createUser(user:UserInput): User
    updateUser(id: ID!, user: UserInput!): User
    deleteUser(id: ID!): User
    
    createProduct(product:ProductInput): Product
    updateProduct(id: ID!, product: ProductInput!): Product
    deleteProduct(id: ID!): Product
    
    createMessage(message:MessageInput): Message
    updateMessage(id: ID!, message: MessageInput!): Message
    deleteMessage(id: ID!): Message
}

schema {
    query: Query
    mutation: Mutation
}
`);

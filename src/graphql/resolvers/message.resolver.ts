import IRepository from "../../interfaces/Repositories/IRepository";

export const MessageResolver = (repository: IRepository) => {
  return {
    messages: async () => {
      try {
        const messagesFetched = await repository.getAll();
        return messagesFetched;
      } catch (err) {
        throw err;
      }
    },
    message: async (args: any) => {
      const { id } = args;
      try {
        const messageFetched = await repository.getById(id);
        return messageFetched;
      } catch (err) {
        throw err;
      }
    },
    createMessage: async (args: any) => {
      const { id, author, date, message } = args.message;
      try {
        const messageCreated = await repository.create({
          id,
          author,
          date,
          message,
        });
        return messageCreated;
      } catch (err) {
        throw err;
      }
    },
    updateMessage: async (args: any) => {
      const id = args.id;
      const payload = args.message;
      try {
        const messageUpdated = await repository.update(id, payload);
        return messageUpdated;
      } catch (err) {
        throw err;
      }
    },
    deleteMessage: async (args: any) => {
      const { id } = args;
      try {
        const deletedUser = await repository.delete(id);
        return deletedUser;
      } catch (err) {
        throw err;
      }
    },
  };
};

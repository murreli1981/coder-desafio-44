import IRepository from "../../interfaces/Repositories/IRepository";

export const UserResolver = (repository: IRepository) => {
  return {
    users: async () => {
      try {
        const usersFetched = await repository.getAll();
        return usersFetched;
      } catch (err) {
        throw err;
      }
    },
    user: async (args: any) => {
      const { id } = args;
      try {
        const userFetched = await repository.getById(id);
        return userFetched;
      } catch (err) {
        throw err;
      }
    },
    createUser: async (args: any) => {
      const { username, hash } = args.user;
      try {
        const userCreated = await repository.create({ username, hash });
        return userCreated;
      } catch (err) {
        throw err;
      }
    },
    updateUser: async (args: any) => {
      const id = args.id;
      const payload = args.user;
      try {
        const userUpdated = await repository.update(id, payload);
        return userUpdated;
      } catch (err) {
        throw err;
      }
    },
    deleteUser: async (args: any) => {
      const { id } = args;
      try {
        const deletedUser = await repository.delete(id);
        return deletedUser;
      } catch (err) {
        throw err;
      }
    },
  };
};

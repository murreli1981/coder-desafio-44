import IRepository from "../../interfaces/Repositories/IRepository";

export const ProductResolver = (repository: IRepository) => {
  return {
    products: async () => {
      try {
        const productsFetched = await repository.getAll();
        return productsFetched;
      } catch (err) {
        throw err;
      }
    },
    product: async (args: any) => {
      const { id } = args;
      try {
        const productFetched = await repository.getById(id);
        return productFetched;
      } catch (err) {
        throw err;
      }
    },
    createProduct: async (args: any) => {
      const { title, price, thumbnail } = args.product;
      try {
        const productCreated = await repository.create({
          title,
          price,
          thumbnail,
        });
        return productCreated;
      } catch (err) {
        throw err;
      }
    },
    updateProduct: async (args: any) => {
      const id = args.id;
      const payload = args.product;
      try {
        const productUpdated = await repository.update(id, payload);
        return productUpdated;
      } catch (err) {
        throw err;
      }
    },
    deleteProduct: async (args: any) => {
      const { id } = args;
      try {
        const deletedUser = await repository.delete(id);
        return deletedUser;
      } catch (err) {
        throw err;
      }
    },
  };
};

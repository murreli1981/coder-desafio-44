import repositories from "../../dal/repositories";
import { UserResolver } from "./user.resolver";
import { ProductResolver } from "./product.resolver";
import { MessageResolver } from "./message.resolver";

const resolvers = {
  ...UserResolver(repositories.userRepository),
  ...ProductResolver(repositories.productRepository),
  ...MessageResolver(repositories.messageRepository),
};

export default resolvers;

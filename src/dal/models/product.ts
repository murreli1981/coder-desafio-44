import { Schema, model } from "mongoose";

const ProductSchema = new Schema({
  title: String,
  price: String,
  thumbnail: String,
});

export default model("Product", ProductSchema);

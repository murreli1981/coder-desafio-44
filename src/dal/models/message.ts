import { Schema, model } from "mongoose";

const MessageSchema = new Schema({
  id: String,
  author: {
    id: String,
    nombre: String,
    apellido: String,
    edad: Number,
    alias: String,
    avatar: String,
  },
  date: String,
  message: String,
});

export default model("Message", MessageSchema);

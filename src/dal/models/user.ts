import { Schema, model } from "mongoose";

const UserSchema = new Schema({
  username: {
    type: String,
    unique: true,
  },
  hash: {
    type: String,
    unique: true,
  },
});

export default model("User", UserSchema);

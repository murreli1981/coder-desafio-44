import IRepository from "../../interfaces/Repositories/IRepository";

export default class BaseRepository implements IRepository {
  private model;
  constructor(model: any) {
    this.model = model;
  }

  async getAll(): Promise<any> {
    return await this.model.find();
  }

  async getById(id: String) {
    const data = await this.model.findById(id);
    return data;
  }

  async getByFilter(filter: any) {
    const data = await this.model.find(filter);
    return data;
  }
  async create(payload: any) {
    const response = await this.model.create(payload);
    return response;
  }
  async update(id: String, payload: any) {
    const response = await this.model.findByIdAndUpdate(id, payload);
    return response;
  }
  async delete(id: String) {
    const response = await this.model.findByIdAndDelete(id);
    return response;
  }
}

import BaseRepository from "../base.repository";

export default class UserRepository extends BaseRepository {
  constructor(UserModel: any) {
    super(UserModel);
  }
}

import BaseRepository from "../base.repository";

export default class MessageRepository extends BaseRepository {
  constructor(MessageModel: any) {
    super(MessageModel);
  }
}

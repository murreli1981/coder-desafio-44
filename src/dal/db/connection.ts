import config from "../../config/globals";
import { connect } from "mongoose";

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

export default class Connection {
  static async getConnection() {
    await connect(config.MONGO_CONN, options);
    console.info(">> DB connected");
  }
}

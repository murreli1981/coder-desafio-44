import dotenv from "dotenv";
dotenv.config();

export default {
  MONGO_CONN: process.env.MONGO_CONN || "",
  PORT: process.env.PORT,
  JWT_SECRET: process.env.JWT_SECRET || "secret",
};

import IRepository from "../../interfaces/Repositories/IRepository";
import BaseService from "../base.service";

export default class MessageService extends BaseService {
  constructor(repository: IRepository) {
    super(repository);
  }
}

import IRepository from "../../interfaces/Repositories/IRepository";
import config from "../../config/globals";
import Jwt from "jsonwebtoken";

export default class AuthService {
  private readonly repository: IRepository;

  constructor(repository: IRepository) {
    this.repository = repository;
  }

  async login(username: string, password: string) {
    const data = await this.repository.getByFilter({
      username,
      hash: password,
    });
    if (data[0]) {
      const { username } = data[0];
      const jwt = Jwt.sign({ username }, config.JWT_SECRET, {
        expiresIn: "5h",
      });
      return { date: new Date(), token: jwt };
    } else return { msg: "not found" };
  }
}

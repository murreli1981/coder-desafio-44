import MessageService from "./modules/message.service";
import ProductService from "./modules/product.service";
import UserService from "./modules/user.service";
import AuthService from "./auth/auth.service";

import repositories from "../dal/repositories";

export default {
  messageService: new MessageService(repositories.messageRepository),
  productService: new ProductService(repositories.productRepository),
  userService: new UserService(repositories.userRepository),
  authService: new AuthService(repositories.userRepository),
};

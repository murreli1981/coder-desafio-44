import MessageController from "./modules/message.controller";
import ProductController from "./modules/product.controller";
import UserController from "./modules/user.controller";
import AuthController from "./auth/auth.controller";

import services from "../services";

export default {
  messageController: new MessageController(services.messageService),
  productController: new ProductController(services.productService),
  userController: new UserController(services.userService),
  authController: new AuthController(services.authService),
};

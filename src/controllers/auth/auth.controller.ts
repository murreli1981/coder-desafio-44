import { Request, Response } from "express";

export default class AuthController {
  private service: any;
  constructor(service: any) {
    this.service = service;
  }

  login = async (req: Request, res: Response, next: Function) => {
    const { username, password } = req.body;
    const data = await this.service.login(username, password);
    res.json(data);
  };
}

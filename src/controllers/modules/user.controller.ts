import { Request, Response } from "express";
import IService from "../../interfaces/Services/IService";
import BaseController from "../base.controller";

export default class UserController extends BaseController {
  constructor(service: IService) {
    super(service);
  }

  login = async (req: Request, res: Response, next: Function) => {
    const credentials = req.body;
    const data = await this.service.getByFilter(credentials);
    res.json(data);
  };
}
